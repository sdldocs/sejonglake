# 시작하기

## 운영 환경 <a id="working-environment"></a>
먼저 SejongLake 운영환경은 다음과 같다.

- OS: Ubuntu ubuntu 20.4.3 LTS \n \l
- DBMS: [postgresql 13.4](https://www.postgresql.org/ftp/)
- 프로그래밍 언어: [Python 3.8.10](https://www.python.org/downloads/release/python-3810/)

또한 Python 환경에서 사용되는 패키지 설치를 위하여 아래 명령을 수행하여야 한다.
```
$ pip install -r requirements.txt
```

postgresDB를 쉽게 접근하기 위한 클라이언트 GUI인 [pgAdmin 4](https://www.pgadmin.org/download/)의 설치도 권장한다. 

> Note:
> 참고로 위에 기술 대부분 SW는 docker container image를 설치하여 개발하였다.

## SejongLake 운영 환경 설정 <a id="setting-sejonglake-working-environmwnt"></a>
데이터레이크 운영을 위하여 다음 3 파일을 설정하여야 한다.

### 데이터 저장 공간 생성 <a id="create-directory-for-datasets"></a>
마지막으로 데이터레이크를 저장할 데이터세트를 저장하는 디렉토리르 생성한다.
```
$ mkdir -m 777 /var/SejongLake/
```

### 데이터레이크 설정 파일 <a id="datalake-configuration-file"></a>
데이터레이크 운영을 위하여 데이터레이크에 저장하는 [데이터세트 저장 공간](#create-directory-for-datasets)과 postgresDB에 카탈로그를 운영하여야 한다. 이를 위한 설정파일 `datalake.ini`을 다음과 같다. 
```
# configuration parameters for Data Lake

# datalake root absolute path
# for production should be 'var'
[datalake_path]
datalake_abs_path=/var/SejongLake/
source_rel_path=Source/
staged_rel_path=Staged/
service_rel_path=Service/

# postgres DB info
[catalog]
host=192.168.8.103
database=sejonglake
user=????????
password=????????
port=5432
#
```

이를 편집기로 열고, `user=`와 `password=`에 각각 SejongLake 운영자의 postgresql `user-id`와 `password`로 채워주고 저장한다.

### 자동 다운로드를 위한 설정 파일 <a id="configurations-for-autodownload"></a>

데이터세트의 데이터 파일을 자동으로 다운로드 받기위한 설정 파일 `Libs/dataset_login.ini`는 다음과 같다.
```
# configuration parameters for SejongLake login
# This contains sensitive data to login site.
# It should be encripted

[login_info]
# infos to login site
.
.
.

[login_info.Z_KAIS_TL_SPBD_BULD_세종]
login_url = https://www.nsdi.go.kr/lxportal/?menuno=2971&
id = ????????
password = ????????
id_selector = #userid
password_selector = #userpasswd
button_selector = #submit_b
.
.
.
```

이를 편집기로 열고, `id =`와 `password=`에 각각 SejongLake 운영자의 다운로드 site의 `user-id`와 `password`로 채워주고 저장한다.

## SejongLake 카탈로그 초기화 <a id="sejonglake-catalog-initialization"></a>

### Python 실행 버추얼 환경 구축 <a id="setting-python-virtual-environment"></a>
SejongLake 운영 프로그램은 앞서 기술한 바와 같이 Python 3.8로 작성되어 있다. 이들에 대한 현재 패키지간의 종속성을 유지하며 운영하기 위하여 구축된 Python 버추얼 환경을 모든 프로그램 수행에 잎서 다음 명령을 수행하여 버추얼 환경을 구축하여야 한다.
```
$ source sejongenv/bin/activate
(sejongenv) $
```

또한 운영 프로그램에서 사용되는 패키지 설치를 위하여 다음 명령을 통하여 **처음 한번만** 다음 명령을 수행하여 패키지들을 설치하여야 한다.
```
$ pip install -r requirements.txt
```

### 카탈로그 구축  <a id="building-catalog"></a>
데이터레이크를 초기화하기 위하여 아래 명령을 수행한다.
```
$ python init_SejongLake.py
```

위 명령의 수행 결과로 아래와 같은 출력이 디스플레이된다.
```
[2022-03-22 09:27:51,384 INFO catalog_config.py:82] >> Created sejonglake database for catalog successfully.
[2022-03-22 09:27:51,402 INFO catalog_config.py:138] >> Created the type file_format_type
[2022-03-22 09:27:51,403 INFO catalog_config.py:138] >> Created the type encoding_type
[2022-03-22 09:27:51,404 INFO catalog_config.py:138] >> Created the type key_type
[2022-03-22 09:27:51,404 INFO catalog_config.py:138] >> Created the type column_data_type
[2022-03-22 09:27:51,429 INFO catalog_config.py:198] >> Created the table datasets for catalog
[2022-03-22 09:27:51,440 INFO catalog_config.py:198] >> Created the table autocollect_files for catalog
[2022-03-22 09:27:51,449 INFO catalog_config.py:198] >> Created the table manualupload_files for catalog
[2022-03-22 09:27:51,455 INFO catalog_config.py:198] >> Created the table source_files for catalog
[2022-03-22 09:27:51,460 INFO catalog_config.py:198] >> Created the table staged_files for catalog
[2022-03-22 09:27:51,465 INFO catalog_config.py:198] >> Created the table meta_files for catalog
[2022-03-22 09:27:51,472 INFO catalog_config.py:198] >> Created the table service_files for catalog
[2022-03-22 09:27:51,481 INFO catalog_config.py:198] >> Created the table columns for catalog
[2022-03-22 09:27:51,487 INFO catalog_config.py:235] >> Created the operational directory for datasets: /var/SejongLake/Source/
[2022-03-22 09:27:51,491 INFO catalog_config.py:235] >> Created the operational directory for datasets: /var/SejongLake/Staged/
[2022-03-22 09:27:51,493 INFO catalog_config.py:235] >> Created the operational directory for datasets: /var/SejongLake/Service/
[2022-03-22 09:27:51,497 INFO catalog_config.py:245] >> Created the temporary directory
[2022-03-22 09:27:51,502 INFO init_SejongLake.py:69] >> Connect to the sejonglake database in init_catalog_table().
[2022-03-22 09:27:51,508 INFO init_catalog_data.py:257] >> Inserted 5 rows into the datasets
[2022-03-22 09:27:51,514 INFO init_catalog_data.py:257] >> Inserted 3 rows into the autocollect_files
[2022-03-22 09:27:51,520 INFO init_catalog_data.py:257] >> Inserted 5 rows into the manualupload_files
[2022-03-22 09:27:51,523 INFO init_catalog_data.py:257] >> Inserted 4 rows into the meta_files
[2022-03-22 09:27:51,541 INFO init_catalog_data.py:257] >> Inserted 31 rows into the service_files
[2022-03-22 09:27:51,594 INFO init_catalog_data.py:257] >> Inserted 97 rows into the column_files
[2022-03-22 09:27:51,613 INFO init_catalog_data.py:257] >> Inserted 39 rows into the column_files
[2022-03-22 09:27:51,629 INFO init_catalog_data.py:257] >> Inserted 30 rows into the column_files
[2022-03-22 09:27:51,652 INFO init_catalog_data.py:257] >> Inserted 48 rows into the column_files
[2022-03-22 09:27:51,687 INFO init_catalog_data.py:257] >> Inserted 74 rows into the column_files
[2022-03-22 09:27:51,709 INFO init_catalog_data.py:257] >> Inserted 48 rows into the column_files
```

PostgresDB에 생성된 SejongLake 카탈로그를 pgAdmin을 이용하여 확인할 수 있다. 아래 그림은 데이터레이크 SejongLake의 카탈로그 테이블 8종을 보여주고 있다.
![init_catalog](../images/fig-01.png)

또한 데이터세트를 저장하는 디렉토리 '/var/SejongLake/'가 다음과 같은 구조로 생성된다.
```
/var/SejongLake
|
|---Source
      |
      |---ITS_표준노드링크
      |---가구통행실태조사
      |---도로명주소_건물
      |---소상공인시장진흥공단_상가_상권
      |---지역별고용조사
|
|---Staged
      |
      |---ITS_표준노드링크
      |---가구통행실태조사
      |---도로명주소_건물
      |---소상공인시장진흥공단_상가_상권
      |---지역별고용조사
|
|---Service
      |
      |---ITS_표준노드링크
      |---가구통행실태조사
      |---도로명주소_건물
      |---소상공인시장진흥공단_상가_상권
      |---지역별고용조사
|
|---Tmp
```

현재 SejongLake에서 관리할 데이터세트 5종을 pgAdmin에서 다음 query로 확인할 수 있다.
```sql
SELECT * 
FROM DATASETS
```

![datasets](../images/fig-02.png)

자동 갱신되는 3종 데이터세트 `도로명주소_건물`, `소상공인시장진흥공단_상가_상권` 및  `ITS_표준노드링크`에 대한 정보는 `autocollect_files` 테이블을 통하여 관리되며, 수동 갱신을 해야하는 데이터세트 2종과 데이터세트에 대한 메타 데이터는 `manualUpload_files`를 통하여 관리된다.

## SejongLake 초기화  <a id="initializing-sejonglake"></a>
이제 데이터레이크 SejongLake의 카탈로그를 구축하였다. 이제 실제 데이터레이트에 첫번째 데이터세트를 저장하여 SejongLake를 구축하여야 한다. 이는 앞서 설명한 것처럼 자동 갱신 데이터세트 3종의 데이터 오브젝트와 수동 갱신 데이터세트 2종의 데이터 오브젝트 및 메타 데이터로 나누어 진행하여야 한다.

### 자동 수집 데이터 오브젝트 첫 적재 <a id="initial-loading-on-autocollect-data-objects"></a>
사용자는 다음과 같은 명령을 수행하여 매우 간단히 처리할 수 있다.
```python
$ python checknUpdate.py
```

위의 명령 처리 결과로 아래와 같은 결과를 표시한다.
```
[2022-03-22 10:27:30,724 INFO downloadFiles.py:178] >> The file Z_KAIS_TL_SPBD_BULD_세종.zip in dataset 도로명주소_건물 is downloaded for update.
[2022-03-22 10:27:55,798 INFO downloadFiles.py:178] >> The file 소상공인시장진흥공단_상가_상권.zip in dataset 소상공인시장진흥공단_상가_상권 is downloaded for update.
[2022-03-22 10:28:18,786 INFO downloadFiles.py:178] >> The file NODELINKDATA.zip in dataset ITS_표준노드링크 is downloaded for update.
[2022-03-22 10:28:43,368 INFO checknUpdate.py:125] >> The file Z_KAIS_TL_SPBD_BULD_세종_20220322.zip in the dataset 도로명주소_건물 is downloaded and updated successfully.
[2022-03-22 10:28:47,869 INFO checknUpdate.py:125] >> The file 소상공인시장진흥공단_상가_상권_20220322.zip in the dataset 소상공인시장진흥공단_상가_상권 is downloaded and updated successfully.
[2022-03-22 10:29:28,455 INFO checknUpdate.py:125] >> The file NODELINKDATA_20220322.zip in the dataset ITS_표준노드링크 is downloaded and updated successfully.
```

적재된 데이터세트는 '/var/SejongLake/Service/[dataset-name]/'에 찾아볼 수 있다. 예를 들어 데이터세트 '소상공인시장진흥공단_상가_상권'의 적재된 파일 '소상공인시장진흥공단_상가_상권_정보_세종.csv'은 '/var/SejongLake/Service/소상공인시장진흥공단_상가_상권/소상공인시장진흥공단_상가_상권_정보_세종.csv'로 접근할 수 있다. 
```shell
$ ll /var/SejongLake/Service/소 상공인시장진흥공단_상가_상권/소상공인시장진흥공단_상가_상권_정보_세종.csv
lrwxrwxrwx 1 yoonjoon.lee yoonjoon.lee 139  3월 22 10:28 /var/SejongLake/Service/소상공인시장진흥공단_상가_상권/소상공인시장진흥공단_상가_상권_정보_세종.csv -> /var/SejongLake/Staged/소상공인시장진흥공단_상가_상권/소상공인시장진흥공단_상가_상권_정보_세종_20220322.csv
``` 

위의 명령을 수행 이후에는 데이터세트의 파일을 사용할 수 있는 상태가 된다. 따라서 해당 파일을 파일 경로 '/var/SejongLake/Service/소상공인시장진흥공단_상가_상권/소상공인시장진흥공단_상가_상권_정보_세종.csv'를 사용하여 데이터 파일을 다운로드하거나 직접 접근할 수 있다.

> Note: <br>
> 디렉토리 '/var/SejongLake/Service/[dataset-name]/'에는 가장 최근에 데이터세트에 적재된 파일의 링크를 저장하고 있으며 이를 통해 실재된 파일이 저장된 디렉토리 '/var/SejongLake/Staged/[dataset-name]/'의 파일에 접근할 수 있다.
> 예를 들면, 위의 '소상공인시장진흥공단_상가_상권_정보_세종.csv'의 경우는 실제 파일 경로는 '/var/SejongLake/Staged/소상공인시장진흥공단_상가_상권/소상공인시장진흥공단_상가_상권_정보_세종_20220322.csv'이다. 이때 파일명의 끝부분  '_20220322'는 데이터가 적재된 년월일을 나타낸다.

이때 postgres DB로 관리되는 카탈로그와 데이터레이크의 변경에 대한 자세한 설명을 [데이터 수집](../reference-guide/data-collection.md)에서 상세히 기술하고 있다. 이를 참고하도록 한다.

### 수동 수집 데이터 첫 적재 <a id="initial-loading-on-manualupload"></a>
앞서 기술한 바와 같이 수동 수집 데이터세트 2종과 메타 데이터 3종은 운영자 또는 사용자가 파일을 해당 웹 사이트로 부터 다운로드 받거나 그 내용을 복사하여 지정된 디렉토리에 저장함으로써 데이터레이크에 적재를 시작할 수 있다. 

> Note: <Br>
> 데이터세트 3종 `도로명주소_건물`, `소상공인시장진흥공단_상가_상권` 및 `지역별고용조사`에 대한 메타 데이터는 각각 독립적으로 해당 사이트에서 다운로드 받거나 복사 저장하여야 한다. 데이터세트 `가구통행실태조사`는 데이터파일과 함께 다운로드되는 파일에 포함되어 있다. 

#### 메터 데이터 첫 적재 <a id="first-loading-on-manualupload-metadata"/></a>

메타 데이터를 수동으로 적재하기 위하여 먼저 다운로드 받아 이들을 해당 데이터세트 디렉토리에 지정된 파일 이름으로 복사 또는 이동 하여야 한다 (또는 지정된 이름으로 다운로드 받는다). 

데이터세트 `도로명주소_건물`의 메타 데이터 `Z_KAIS_TL_SPBD_BULD.xlsx`를 웹 사이트 `http://data.nsdi.go.kr/dataset/14783`에서 다운로드 받는다. 다운로드 받은 파일을 디렉토리 `/var/SejongLake/Source/도로명주소_건물/`에 복사하거나 이동시킨다.
```shell
$ cp [download path] /var/SejongLake/Source/도로명주소_건물/Z_KAIS_TL_SPBD_BULD.xlsx
```

```shell
$ ll /var/SejongLake/Source/도로명주소_건물/Z_KAIS_TL_SPBD_BULD.xlsx
-rwxrwxr-x 1 yoonjoon.lee yoonjoon.lee 162463  3월 22 15:59 /var/SejongLake/Source/도로명주소_건물/Z_KAIS_TL_SPBD_BULD.xlsx
```
복사되었음을 확인한 다음 아래 명령을 수행하여 데이터레이트에 메타 데이터를 적재한다.
```
$ python manualUpdate.py -m 도로명주소_건물 Z_KAIS_TL_SPBD_BULD.xlsx
```

정상적으로 수행된 결과는 다음과 같다.
```
[2022-03-22 16:05:50,030 INFO manualUpdate.py:221] >> Z_KAIS_TL_SPBD_BULD.xlsx in 도로명주소_건물 just uploaded successfully.
```

> Note: <br>
> 현 버전에는 제공되는 메터데이터가 데이터 오브젝트 처리에 불충분하여 보관의 의미만 있어 사용자는 이에 대한 접근을 할 수 없다.

데이터세트 `소상공인시장진흥공단_상가_상권`의 메타 데이터 `소상공인시장진흥공단_상가_상권_meta.json`를 웹 사이트 `https://www.data.go.kr/catalog/15083033/fileData.json`에서 열고 이를 복사하여  받는다. 다운로드 받은 파일을 디렉토리 `/var/SejongLake/Source/소상공인시장진흥공단_상가_상권/`에 파일 `소상공인시장진흥공단_상가_상권_meta.json`를 생성한다.

```shell
$ ll /var/SejongLake/Source/소상공인시장진흥공단_상가_상권/소상공인시장진흥공단_상가_상권_meta.json
-rwxrwxr-x 1 yoonjoon.lee yoonjoon.lee 987  3월 22 16:36 소상공인시장진흥공단_상가_상권_meta.json
```
복사되었음을 확인한 다음 아래 명령을 수행하여 데이터레이트에 메타 데이터를 적재한다.
```
$ python manualUpdate.py -m 소상공인시장진흥공단_상가_상권 소상공인시장진흥공단_상가_상권_meta.json
```

정상적으로 수행된 결과는 다음과 같다.
```
[2022-03-22 16:40:29,808 INFO manualUpdate.py:221] >> 소상공인시장진흥공단_상가_상권_meta.json in 소상공인시장진흥공단_상가_상권 just uploaded successfully.
```

데이터세트 `지역별고용조사`의 메타 데이터 `2021_지역별고용조사_파일설계서.xlsx`를 웹 사이트 `https://mdis.kostat.go.kr/index.do`에서 에서 다운로드 받는다. 다운로드 받은 파일을 디렉토리 `/var/SejongLake/Source/지역별고용조사/`에 파일 `지역별고용조사_파일설계서.xlsx`으로 복사하거나 이동시킨다.

```shell
$ ll /var/SejongLake/Source/지역별고용조사/지역별고용조사_파일설계서.xlsx
-rw-rw-r-- 1 yoonjoon.lee yoonjoon.lee 60640  3월 22 16:59 /var/SejongLake/Source/지역별고용조사/지역별고용조사_파일설계서.xlsx
```
복사되었음을 확인한 다음 아래 명령을 수행하여 데이터레이트에 메타 데이터를 적재한다.
```
$ python manualUpdate.py -m 지역별고용조사 지역별고용조사_파일설계서.xlsx
```

정상적으로 수행된 결과는 다음과 같다.
```
[2022-03-22 17:01:41,929 INFO manualUpdate.py:221] >> 지역별고용조사_파일설계서.xlsx in 지역별고용조사 just uploaded successfully.
```

#### 수동 수집 데이터 오브젝트 첫 적재 <a id="first-loading-on-manualupload-datasets"></a>
데이터를 수동으로 적재하기 위하여 먼저 다운로드 받아 이들을 해당 데이터세트 디렉토리에 지정된 파일 이름으로 복사 또는 이동 하여야 한다 (또는 지정된 이름으로 다운로드 받는다). 

데이터세트 `지역별고용조사`의 데이터 `지역별고용조사(지역별고용조사_하반기(A형_시군_중분류)(제공)_20201006_90177_데이터).zip`를 웹 사이트 `https://mdis.kostat.go.kr/index.do`에서 다운로드 받는다. 다운로드 받은 파일을 디렉토리 `/var/SejongLake/Source/지역별고용조사/`에 파일 '지역별고용조사_데이터.zip'으로 복사하거나 이동시킨다.
```shell
$ cp [download path] /var/SejongLake/Source/지역별고용조사/지역별고용조사_데이터.zip
```

```shell
$ ll /var/SejongLake/Source/지역별고용조사/지역별고용조사_데이터.zip
-rwxrwxr-x 1 yoonjoon.lee yoonjoon.lee 4549898  3월 22 17:16 /var/SejongLake/Source/지역별고용조사/지역별고용조사_데이터.zip
```
복사되었음을 확인한 다음 아래 명령을 수행하여 데이터레이트에 메타 데이터를 적재한다.
```
$ python manualUpdate.py -d 지역별고용조사 지역별고용조사_데이터.zip
```

정상적으로 수행된 결과는 다음과 같다.
```
[2022-03-22 17:18:32,234 INFO manualUpdate.py:221] >> 지역별고용조사_데이터.zip in 지역별고용조사 just uploaded successfully.
```

#### 수동 수집 데이터 오브젝트와 메타 데이터 첫 적재 <a id="first-loading-on-manualupload-datasets-and-metadata"></a>

데이터세트 `가구통행실태조사`의 데이터와 메타 데이터 모두를 포함하는 파일  `가구통행실태조사(2016년기준)_190201.zip`를 웹 사이트 `https://www.ktdb.go.kr/www/contents.do?key=202`에서 다운로드 받는다. 다운로드 받은 파일을 디렉토리 `/var/SejongLake/Source/가구통행실태조사/`에 파일 '가구통행실태조사.zip'으로 복사하거나 이동시킨다.
```shell
$ cp [download path] /var/SejongLake/Source/가구통행실태조사/가구통행실태조사.zip
```

```shell
$ ll /var/SejongLake/Source/가구통행실태조사/가구통행실태조사.zip
-rwxrwxr-x 1 yoonjoon.lee yoonjoon.lee 58291701  3월 23 08:33 가구통행실태조사.zip
```
복사되었음을 확인한 다음 아래 명령을 수행하여 데이터레이트에 메타 데이터를 적재한다.
```
$ python manualUpdate.py -x 가구통행실태조사 가구통행실태조사.zip
```

정상적으로 수행된 결과는 다음과 같다.
```
[2022-03-23 08:36:04,639 INFO manualUpdate.py:221] >> 가구통행실태조사.zip in 가구통행실태조사 just uploaded successfully.
```

이상으로 초기 데이터 적재를 완료하였다. 이후부터 데이터레이크를 운영하는 단계에 들어가는 것이다. 

## 데이터레이크 운영 <a id="datalake-update"></a>
앞서 기술한 바와 같이 3종 자동 수집 데이터세트 `도로명주소_건물`, `소상공인시장진흥공단_상가_상권` 및  `ITS_표준노드링크`의 파일은 주기적으로(예를 들어 매일, 매주 또는 매달) 사이트에서 변경 여부를 확인하여 이를 사이트로부터 자동으로 다운로드 받아 데이터 오브젝트(파일)을 변경된다. 그러나 수동 수집 데이터세트 2종 `지역별고용조사`와 `가구통행실태조사`의 데이타와 메타 데이타는 운영자가 그 변경을 확인하여 그 변경을 데이터세트에 반영하여야 한다. 

### 자동 수집 데이터 오브젝트 갱신 설정 <a id="setting-autocollect-data-objects-update"></a>
자동 수집 데이터세트의 갱신을 위하여는 데이터 제공 사이트의 갱신 여부 확인 주기를 설정하여 주기적으로 확인한다. 확인 `crontab`을 이용하여 이를 시스템에 등록하여야 한다 예를 들어 매일 새벽 5:00 AM에 갱신 여부를 확인하여 자동 수집 데이터세트를 갱신하고자 한다면, `crontab -e` 명령을 수행하고 편집기를 이용하여 `0 5 * * * /[실행파일경로]`를 삽입한다. 삽입 후 `crontab -l`을 실행하여 설정되어 있는 지를 확인한다.
```
0 5 * * *  /home/yoonjoon.lee/Prototype/proto_datalake/autocollect.sh
```

매일 오전 5시 정각에 정기적으로 `/home/yoonjoon.lee/Prototype/proto_datalake/autocollect.sh`을 실행시켜 갱신 여부를 확인한다.

만약 변경이 없다면 다음과 같은 결과를 출력한다.
```
[2022-03-23 10:52:21,188 INFO checknUpdate.py:90] >> No update on sources: 03/23/2022, 10:52:21
```

반대로 변경이 있었다면 다음과 같은 결과를 출력한다.
```
```

위의 출력 메시지는 콘솔에 출력될 뿐만아니라 로그파일에 저장되므로 `tail /home/yoonjoon.lee/Prototype/proto_datalake/datalake.log'을 통하여 확인할 수 있다.

```
.
.
.
[2022-03-23 10:52:21,188 INFO checknUpdate.py] >> No update on sources: 03/23/2022, 10:52:21
[2022-03-23 10:59:39,992 INFO checknUpdate.py] >> No update on sources: 03/23/2022, 10:59:39
```

### 수동 수집 데이터오브젝트 및 메타 데이터 변경 <a id="manualipload-data-objects-update"></a>
수동 수집 데이터세트의 데이타와 메타 데이타는 운영자가 그 변경을 확인하여 [수동 수집 데이터 오브젝트 첫 적재](#first-loading-on-manualupload-datasets-and-metadata)와 동일한 절차에 따라 그 변경을 데이터세트에 반영하여야 한다. 

## 데이터레이크 시각화 <a id="datalake-visualization"></a>
데이터레이크에 데이터를 적재한 후 적재된 데이터에 대한 간단한 가시화 기능을 제공한다. 이는 본격적인 데이터 분석 기능을 제공하기 보다는 데이터에 대한 요약 통계를 보여주어 사용자에게 데이터의 개략적인 분포를 웹을 통하여 보여주는 것이 목적이다.

다음과 같은 명령을 수행하여 시작한다.
```
$ python visualize.py
 * Serving Flask app 'visualize' (lazy loading)
 * Environment: production
   WARNING: This is a development server. Do not use it in a production deployment.
   Use a production WSGI server instead.
 * Debug mode: off
[2022-03-23 12:11:29,845 WARNING _internal.py:225] >>  * Running on all addresses.
   WARNING: This is a development server. Do not use it in a production deployment.
[2022-03-23 12:11:29,845 INFO _internal.py:225] >>  * Running on http://192.168.8.103:5000/ (Press CTRL+C to quit)
```

위 결과에서 `http://192.168.8.103:5000/`를 브라우저의 주소창에 복사하면 다음과 같은 화면을 얻을 수 있다.

![vis-fig01](../images/fig-03.png)

위의 그림과 같이 최근 갱신된 데이터세트들 순으로 표시된다. `datalake.log`를 화인하여 갱신된 데이터세트가 있으면 이를 통해 갱신된 데이터들의 간략한 통계 정보를 얻을 수 있다.

> Note: <br>
> 현재 버전에서는 데이터세트 `소상공인진흥공단_상가_상권`에 대한 가시화만 구현되어 있으므로 이를 중심으로 설명한다.

`소상공인진흥공단_상가_상권`를 클릭하면 다음과 같은 화면으로 이동한다.

![vis-fig02](../images/fig-04.png)

`Category by column`의 drop down 메뉴에서 `행정동`을 선택하고 `submit` 버튼을 클릭하여 다음과 같은 행정동별 상가 정보를 볼 수 있다.

![vis-fig03](../images/fig-05.png)

브라우저의 뒤로 돌아가기를 클릭한 다음 `Category by column`의 drop down 메뉴에서 `상권업종분류`을 선택하고 `submit` 버튼을 클릭하여 다음과 같은 부메뉴를 볼 수 있다.

![vis-fig04](../images/fig-06.png)

이때 선택없이 `submit` 버튼을 클릭하면 다음과 같은 업종 대분류에 따른 요액 통계를 볼 수 있다.

![vis-fig05](../images/fig-07.png)

브라우저의 뒤로 돌아가기를 클릭한 다음 대분류에서 `음식`을 선택하고 `submit` 버튼을 클릭하면 다음과 같이 `음식` 업종의 중분류에 따른 요액 통계를 출력한다.
![vis-fig06](../images/fig-08.png)

브라우저의 뒤로 돌아가기를 클릭한 다음 대분류에서 `음식`은 이미 선택되아 있고 중분류에서 `한식`을 더 선택한 다음 `submit` 버튼을 클릭하면 다음과 같이 대분류가 `음식` 업종이며, 중분류가 `한식`인 소분류에 따른 요액 통계를 출력한다.
![vis-fig07](../images/fig-09.png)

> Note: <br>
> 현재 버전에서는 다중 속성별 요약통계 기능은 제공하지 않는다. 이는 분석용 소프트웨어를 이용하여 데이터레이크로 부터 데이터세트를 원격으로 읽거나 다운로드받아 로컬에서 읽어 더 전문적인 데이터 분석을 수행하여야 한다.
