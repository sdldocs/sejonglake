# 데이터 시각화

이미 [데이터레이크 시각화](../../user-guide/getting-started/#datalake-visualization)에서 밝혔듯이 시각화의 목적은 데이터레이크에 데이터를 적재한 후 적재된 데이터에 대한 검증과 본격적인 데이터 분석 기능을 제공하기 보다는 데이터에 대한 요약 통계를 보여주어 사용자에게 데이터의 개략적인 분포를 보여주는 것이 목적이다. 따라서 아직 전체적인 구조와 기능보다는 프로타입의 실연에 중점을 두었다. 향후 기술한 현재 ver 0.1에서는 `SejongLake`의 데이터세트 `소상공인진흥공단_상가_상권`에 대한 가시화만 구현하였다. 

구현은 [Flask](https://flask.palletsprojects.com/en/2.1.x/) 프레임워크를 사용하여 웹 어플리케이션으로 구현하였고, 해당 코드는 `visulaize.py`와 이에 필요한 함수는 `VisUtil/`에 정의되어 있다. 또한 가시화 대상 속성은 `smb_fields.txt`에 정의되어 있으나 이는 향후 toml이나 yaml 형식으로 변환하여야 한다. 또는 카탈로그 테이블 `columns_file`에 해당 정보를 저장할 수 있도록 한다. 플롯팅을 위하여 [`Altair`](https://altair-viz.github.io/) 패키지를 이용하였다. 또한 웹 페이지 랜더링을 위하여 javascript도 사용하였다. 정적 사이트를 위한 폴더 `static/`과 웹 페이지 템플리트을 저장한 `templates/` 폴더가 있다.
