# 데이터 수집 
현재 버전(version 0.1)의 수집 대상은 공공 데이터 사이트(https://data.go.kr)로 한정하고 있다. 따라서 모든 데이터 오브젝트는 소스 사이트로 부터 다운로드하여 이를 데이터레이크에 저장한다. 그러나 [데이터레이크 생성](../catalog/#create-datalake)에서 밝혔듯이 자동 갱신과 수동 갱신 데이터 오브젝트로 나누어 수집한다. 자동 갱신을 위한 코드는 디렉토리 `PullDataSet/`에서 찾아 볼 수 있으며, 수동 갱신을 위한 코드는 `manualUpdate.py`로 부터 출발한다.

데이터 수집은 [데이터 첫 적재](../../user-guide/getting-started/#initializing-sejonglake)를 위한 수집과 이후 [데이터레이크 운영에 따른 수집](../../user-guide/getting-started/#datalake-update)으로 나룰 수 있다<a href="#foot-1" id="foot-1-ref"><sup>1</sup></a>. 첫 수집과 갱신을 위항 수집은 데이터 오브젝트의 최근 적재 시각애 따라 다르게 수행되기 때문이다. 따라서 데이터 수집은 앞서 언급한 바와 같이 다음과 같아 분류할 수 있다.

- [첫 자동 적재를 위한 수집](../../user-guide/getting-started/#initial-loading-on-autocollect-data-objects)
- [첫 수동 적재를 위한 수집](../../user-guide/getting-started/#initial-loading-on-manualupload)
- [자동 갱신을 위한 수집](../../user-guide/getting-started/#setting-autocollect-data-objects-update)
- [수동 갱신을 위한 수집](../../user-guide/getting-started/#manualupload-data-objects-update)

그러나 데이터 오브젝트의 경우 첫 자동 적재와 갱신을 위한 수집 모두 `chceknUpdate.py`에 의하여 수행되며 차이는 단순히 호출 주체의 차이뿐이다. 즉 첫 수집은 데이터레이크 초기화 이후 `$ python checknUpdate.py`를 수동으로 수행하여 데이터레이크 초기 데이터 수집할 수 있다. 자동 갱신을 위한 수집을 위하여 갱신 검사 주기를 설정하여 주기적으로 자동 수집할 수 있다.  검사 주기에 따라 `checknUpdate.py` 프로그램을 자동으로 수행하여 데이터 오브젝트의 갱신 여부를 확인하고 갱신이 되었다면 데이터 오브젝트에 대한 수집을 수행한다. 

데이터 오브젝트의 수동 수집의 필요성에 대하여 [프로젝트 개요](../../../sejonglake/)에서 기술하였다.  첫 수동 적재와 갱신을 위한 수집은 내부적으로 차이가 없다. 수동 수집를 기술하도록 한다.  그 대상이 되는 데이터 오브젝트는 두 종류가 있다. 하나는 데이터 오브젝트의 구조를 기술하는 메타 데이터<a href="#foot-2" id="foot-2-ref"><sup>2</sup></a>이고, 다른 것은 실제 데이터 오브젝트들이다. 또한 두 종류를 모두 포함한 데이터 오브젝트도 존재한다. 따라서 실제로 세 종류의 데이터 오브젝트 타입에 따른 처리가 요구된다. 

먼저 공통적인 사항으로 적재할 데이터 오브젝트를 웹사이트에서 다운로드 받는다. 다운로드받은 파일(데이터 오브젝트)을 데이터 오브젝트의 서비스 명(파일 명)으로 미리 설정된 디렉토리(예: 에 `/var/SejongLake/Source/[dataset-name]/`)에 복사한다. 이후 데이터 오브젝트의 타입에 따라 다음 세 명령 중 하나을 실행하여 데이터레이크에 적재할 수 있다. 먼저 데이터 타입이 [메타 데이터이면](../../user-guide/getting-started/#first-loading-on-manualupload-metadata) `python manualUpdate.py -m 도로명주소_건물 Z_KAIS_TL_SPBD_BULD.xlsx`, [데이터 타입이 데이터이면](../../user-guide/getting-started/#first-loading-on-manualupload-datasets) `python manualUpdate.py -d 도로명주소_건물 Z_KAIS_TL_SPBD_BULD.xlsx`과 [데이터 타입이 데이터와 메타 데이터가 같이 있다면](../../user-guide/getting-started/#first-loading-on-manualupload-datasets-and-metadata) `python manualUpdate.py -x 도로명주소_건물 Z_KAIS_TL_SPBD_BULD.xlsx`를 각각 실행한다.

앞서 기술한 것에 알 수 있듯이 자동과 수동 수집의 차이점은 데이터에 적재할 데이터 오브젝트의 어떻게 데이터 오브젝트에 대한 서비스를 위한 데이터 준비 단계 처리를 데이터 오브젝트를 제공하는 것이다. 즉 지정된 디렉토리 `/var/SejongLake/Source/`내 해당 데이터세트 디렉토리에 데이터 오브젝트를 다운로드(자동 적재)하거나 복사(수동 적재)하는 것이다. 

자동 수집를 위하여 주기적으로 웹 사이트를 방문하여 갱신 여부를 확인하여야 한다. 따라서 웹사이트에 게시된 데이터를 읽을 수 있어야 한다. 이를 위하여 [selenium WebDriver](https://www.selenium.dev/documentation/webdriver/)를 이용하였다. 또한 자동 로그인을 포함하여 데이터를 접근하는 데 필요한 데이터를 toml로 `Libs/dataset_login.ini`에 기술되어 있다. 
```toml
# configuration parameters for SejongLake login
# This contains sensitive data to login site.
# It should be encripted

[login_info]
# infos to login site

[login_info.Z_KAIS_TL_SPBD_BULD]
login_url = https://www.nsdi.go.kr/lxportal/?menuno=2971&
id = #adminid
password = #adminpassword
id_selector = #userid
password_selector = #userpasswd
button_selector = #submit_b

[login_info.Z_KAIS_TL_SPBD_BULD_세종]
login_url = https://www.nsdi.go.kr/lxportal/?menuno=2971&
id = yoonjoon.lee@gmail.com
password = suredata1908@
id_selector = #userid
password_selector = #userpasswd
button_selector = #submit_b

# site url to login
[site_url]
z_kais_tl_spbd_buld = http://data.nsdi.go.kr/dataset/14783
z_kais_tl_spbd_buld_세종 = http://data.nsdi.go.kr/dataset/14783
소상공인시장진흥공단_상가_상권 = https://www.data.go.kr/data/15083033/fileData.do
NODELINKDATA = https://www.its.go.kr/nodelink/nodelinkRef

# download resources
[download_button_selector]
z_kais_tl_spbd_buld = #data_table_resource > tbody > tr:nth-child(2) > td:nth-child(5) > ul > li > button
Z_KAIS_TL_SPBD_BULD_세종 = #data_table_resource > tbody > tr:nth-child(10) > td:nth-child(5) > ul > li > button
소상공인시장진흥공단_상가_상권 = #contents > div.data-search-view > div.data-set-title > div.btn-util > div > a.button.mb-3
소상공인시장진흥공단_상가상권_meta = #contents > div.data-search-view > h3 > small > a
NODELINKDATA = #data_file_list_result > li:nth-child(1) > a.down
.
.
.
```

데이터 수집의 마지막 단계는 카탈로그를 갱신하는 것이다. 즉 수집한 데이터 오브젝트가 저장된 데이터세트 디렉토리의 `/var/[datalake]/Source/[dataset]/[data-object]`를 카탈로그 테이블에 `source-files`에 등록하는 것이다.

----
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a> version 0.1 세종시 도시행정을 위한 디지털 트윈 구축에 필요한 데이터 중 웹사이트에 공개된 데이터 세트의 최신 버전을 유지하는 것이 그 목적이다. 그러나 다운로드되는 데이터 오브젝트 내부 데이터에는 데이터의 작성 (transaction time)또는 유효 기간(valid time)이 기록되어 있지 않다. 변경된 데이터만을 갱신하는 데 많은 시간이 필요하다. 따라서 데이터 오브젝트 전체를 다운로드 받아 이를 적재하여 저장한다. 

<a id="foot-2" href="#foot-2-ref"><sup>2</sup></a> version 0.1 세종시 도시행정을 위한 디지털 트윈 구축에서는 메터 데이터에 대한 기술이 처리 필요한 정보를 충분히 기수하지 못하고 있어 실제 처리에 사용되지 않고 있다. 그러나 향후 확장에 필요하여 적재는 할 수 있도록 구현하였다.