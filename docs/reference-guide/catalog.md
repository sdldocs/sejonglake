# 카탈로그 관리

## 데이터레이크 생성 <a id="create-datalake"></a>
데이터레이크의 데이터 오브젝트를 저장하는 데이터데이터 자장소를 먼저 지정하여야 한다. 

데이터레이크에 저장되는 모든 데이터세트와 데이터 오브젝트에 관한 정보 뿐만아니라 데이터 수집에서부터 데이터 제공에 이르는 뤄크플로우에서 데이터 처리에 관한 정보를 저장하고 관리하여 SureDataOps 운영을 지원한다. DBMS `postgreSQL`를 이용하여 데이터베이스에 저장 관리한다. 데이터레이크 인스탄스마다 데이터베이스를 생성한다. 현재 버전(version 0.1)에서는 datasets, autocollect_files, manualUpload_files, source_files, staged_files, meta_files, service_files과 column_files 테이블을 생성한다<a href="#foot-1" id="foot-1-ref"><sup>1</sup></a>.

위의 테이블들은 [`$ python init_SejongLake.py`](../../user-guide/getting-started/#initializing-sejonglake)의 시작 단계에서 생성된다. 각 테이블에 대하여 간단한 살명은 다음과 같다.

- `datasets`: 데이터레이크에서 저장 관리하는 모든 데이터세트 정보
- `autocollect_files`: 데이터 오브젝트의 소스가 갱신되면 이를 자동으로 수집하기 위한 정보
- `manualUpload_files`: 데이터 오브젝트<a href="#foot-2" id="foot-2-ref"><sup>2</sup></a>의 소스 갱신 주기가 매우 길어 (예: 1년 이상) 데이터 오브젝트를 다운로드하여 수동으로 데이터레이크에 적재하기 위한 정보
- `source_files`: 수집된 데이터 오브젝트의 접근 경로 및 그 이력
- `staged_files`: 준비된 데이터 오브젝트의 접근 경로 정보 및 그 이력
- `meta_files`: 데이터 오브젝트에 대한 메타 정보 (예 다운로드 url 등)
- `service_files`: 데이터레이크의 제공하는 데이터 오브젝트 정보, 최신 갱신 일시, 최신 데이터 오브젝트 링크 등
- `column_files`: 데이터 오브젝트의 속성(attribute) 정보

이를 위하여 데이터레이크 [설정 파일](../tech-debts/#catalog-1), root 디렉토리의 `datalake.ini`에 데이터 저장소와 카탈로그를 저장하는 데이터베이스에 관한 장보를 [toml](https://pypi.org/project/toml/0.9.0/)형식으로 기술하여야 한다. 다음은 `SejongLake` 데이터레이크 인스탄스를 생성하기 위한 `datalake.ini` 파일 내용이다.
```toml
# configuration parameters for Data Lake

# datalake root absolute path
[datalake_path]
datalake_abs_path=/var/SejongLake/
source_rel_path=Source/
staged_rel_path=Staged/
service_rel_path=Service/

# postgres DB info
[catalog]
host=192.168.8.103
database=sejonglake
user=postgres
password=####
port=5432
```

4번째 행의 `[datalake_path]`는 [데이터 저장소의 경로<a href="#foot-3" id="foot-3-ref"><sup>3</sup></a>](../../user-guide/getting-started/#building-catalog)를 표사한다. `[catalog]`에는 카탈로그를 저장하는 데이터베이스 connection을 위한 정보이다. 즉 `sejonglake`라는 postgreSQL 데이터베이스를 카탈로그 관리를 위하여 사용하며 이를 사용하기 위한 정보이다.

위의 설정 파일로 부터 데이터 저장소와 빈 데이터베이스를 생성한다. 이제 카탈로그 저장을 위한 위에서 설명한 테이블들을 생성하여야 한다. 이 생성을 위한 정보는 `Init_DataLake/catalog_schema.ini`에 toml 형식으로 설정되어 있다. 그 내용은 다음과 같다.
```toml
# Catalog schema definitions
# domain definitions for catalog
[types]
file_format_type = CREATE TYPE file_format_type AS ENUM ('zip', 'xlsx', 'xls', 'csv', 'dbf', 'prj', 'sbn', 'sbx', 'shp', 'shx', 'json', 'hwp');
encoding_type = CREATE TYPE encoding_type AS ENUM ('ascii', 'utf-8', 'euc-kr', 'bin');
key_type = CREATE TYPE key_type AS ENUM ('PK', 'FK');
column_data_type = CREATE TYPE column_data_type AS ENUM ('NUMERIC', 'TEXT');

# table schema for catalog
[tables]
# Datasets
datasets = CREATE TABLE IF NOT EXISTS datasets(
  dataset_id uuid DEFAULT gen_random_uuid(),
  dataset_name varchar(256) UNIQUE,
  source_URL text,
  creation_date timestamp,
  auto_collect  boolean,
  login4collect boolean,
  last_access_time timestamp,
  number_of_access BIGINT,
  description text,
  last_update_time timestamp,
  PRIMARY KEY (dataset_id)
  );

# AutoCollect_Files
autocollect_files = CREATE TABLE IF NOT EXISTS autocollect_files(
  file_id uuid DEFAULT gen_random_uuid(),
  file_name varchar(256),
  source_URL text,
  dataset_id uuid references datasets(dataset_id),
  file_format file_format_type,
  encoding  encoding_type,
  update_cycle  int,
  last_download_time timestamp,
  description text,
  last_update_time timestamp,
  PRIMARY KEY (file_id)
  );

# ManualUpload_Files
manualupload_files = CREATE TABLE IF NOT EXISTS manualupload_files(
  file_id uuid DEFAULT gen_random_uuid(),
  file_name varchar(256),
  source_URL text,
  dataset_id uuid references datasets(dataset_id),
  file_format file_format_type,
  encoding  encoding_type,
  last_upload_time timestamp,
  description text,
  last_update_time timestamp,
  PRIMARY KEY (file_id)
  );

# Source_Files
source_files = CREATE TABLE IF NOT EXISTS source_files(
  file_id uuid DEFAULT gen_random_uuid(),
  file_name varchar(256),
  dataset_id uuid references datasets(dataset_id),
  file_format file_format_type,
  encoding  encoding_type,
  load_time  timestamp,
  last_update_time timestamp,
  PRIMARY KEY (file_id)
  );

# Staged_Files
staged_files = CREATE TABLE IF NOT EXISTS staged_files(
  file_id uuid DEFAULT gen_random_uuid(),
  file_name varchar(256),
  source_file_id uuid references source_files(file_id),
  file_format file_format_type,
  encoding  encoding_type,
  staged_time timestamp,
  last_update_time timestamp,
  PRIMARY KEY (file_id)
  );

# Meta_files
meta_files = CREATE TABLE IF NOT EXISTS meta_files(
  file_id uuid DEFAULT gen_random_uuid(),
  file_name varchar(256),
  staged_file_id uuid references staged_files(file_id),
  file_format file_format_type,
  encoding  encoding_type,
  load_time timestamp,
  last_update_time timestamp,
  PRIMARY KEY (file_id)
  );

# Service_Files
service_files = CREATE TABLE IF NOT EXISTS service_files(
  file_id uuid DEFAULT gen_random_uuid(),
  file_name varchar(256),
  meta_file_id uuid references meta_files(file_id),
  staged_file_id uuid references staged_files(file_id),
  dataset_id uuid references datasets(dataset_id),
  file_format file_format_type,
  encoding  encoding_type,
  number_of_records  BIGINT,
  number_of_fields  INT,
  ready_date  timestamp,
  last_update_time timestamp,
  PRIMARY KEY (file_id)
  );

# Column_Files
columns = CREATE TABLE IF NOT EXISTS column_files(
  column_id uuid DEFAULT gen_random_uuid(),
  column_name varchar(256),
  service_file_id uuid references service_files(file_id),
  key   key_type,
  domain  varchar(256),
  column_type    column_data_type,
  length  int,
  null_allowed  boolean,
  defaultvalue  varchar(256),
  description   text,
  last_update_time timestamp,
  PRIMARY KEY (column_id)
  );
```

위 toml 파일의 첫번째 table `[types]`는 데이터베이스 테이블의 열들의 도메인을 정의한 것이며, 두번쩨 table '[tables]`는 카탈로그 테이블을 생성하는 SQL문이다.

이제 datalake 인스탄스에 따라 데이터세트와 데이터 오브젝트를 생성하여 카탈로그를 완성하여야 한다.

## 데이터세트 생성 <a id="create-dataset"></a>
version 0.1에서는 이미 `sejonglake`에 특화된 카탈로그를 생성하였다. 이는 `Init_Proto_DataLake` 디렉토리내의 json 파일 형태로 저장된 파일을 읽어 카탈로그 테이블을 초기화한다. 각 데이터세트 초기화 데이터는 `datasets_init.json`에 다음과 같이 저장되어 있다.
```json
[
  {
    "dataset_name": "도로명주소_건물",
    "source_URL": "http://data.nsdi.go.kr/dataset/14783",
    "creation_date": null,
    "auto_collect": true,
    "login4collect": true,
    "last_access_time": null,
    "number_of_access": 0,
    "description": "세종시 건축물의 사용승인 전 도로명주소 부여를 위해 생성되는 건물 정보",
    "last_update_time": null
  },
  {
    "dataset_name": "소상공인시장진흥공단_상가_상권",
    "source_URL": "https://www.data.go.kr/tcs/dss/selectFileDataDetailView.do?publicDataPk=15083033",
    "creation_date": null,
    "auto_collect": true,
    "login4collect": false,
    "last_access_time": null,
    "number_of_access": 0,
    "description": "세종시에서 영업 중인 전국 상가업소 데이터 (상호명, 업종코드, 업종명, 지번주소, 도로명주소, 경도, 위도 등)",
    "last_update_time": null
  },
  {
    "dataset_name": "지역별고용조사",
    "source_URL": "https://mdis.kostat.go.kr/extract/extYearsSurvSearch.do?curMenuNo=UI_POR_P9012",
    "creation_date": null,
    "auto_collect": false,
    "login4collect": true,
    "last_access_time": null,
    "number_of_access": 0,
    "description": "세종시 MDIS 지역별고용조사",
    "last_update_time": null
  },
  {
    "dataset_name": "ITS_표준노드링크",
    "source_URL": "https://www.its.go.kr/nodelink/nodelinkRef",
    "creation_date": null,
    "auto_collect": true,
    "login4collect": false,
    "last_access_time": null,
    "number_of_access": 0,
    "description": "전국 표준노드링크 및 회전, 중용정보",
    "last_update_time": null
  },
  {
    "dataset_name": "가구통행실태조사",
    "source_URL": "https://www.ktdb.go.kr/www/contents.do?key=202",
    "creation_date": null,
    "auto_collect": false,
    "login4collect": true,
    "last_access_time": null,
    "number_of_access": 0,
    "description": "세종시 가구통행 실태 조사",
    "last_update_time": null
  }
]
```

향후, 이는 [SQL의 DDL과 같은 명령이나 적어도 API 호출 형식으로 변환](../tech-debts)되어 사용성을 제고하여야 한다.

> **Note**: 테이블 `column_files`에 대한 정보는 데이터 오브젝트 속성(attribute)가 많아 `column_files_init.json`, `column_init_BUILD.json`, `column_init_EMP.json`, `column_init_MOCT.json`, `column_init_SMB.json`, `column_init_TRAFICM.json`과 `column_init_TRAFICP.json`에 초기화 데이터를 나누어 저장하였다. 


## 데이터 오브젝트 생성 <a id="create-data-object"></a>
[데이터 오브젝트에 대한 정보](#create-datalake)는 그 참조에 따라 `autocollect_files`, `manualUpload_files`, `source_files`, `staged_files`, `meta_files`과 `service_files`에 나누어 저장되고 이들은 각각 `autocollect_files_init.json`, `manualUpload_files_init.json`, `source_files_init.json`, `staged_files_init.json`, `meta_files_init.json`과 `service_files_init.json`에 초기화 데이터를 저장하고 있다. `service_files_init.json` 파일에 저장된 초기화 데이터는 다음과 같다.
```json
[
  {
    "file_name": "Z_KAIS_TL_SPBD_BULD_36110.dbf",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "dbf",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  30,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "도로명주소_건물"
  },
  {
    "file_name": "Z_KAIS_TL_SPBD_BULD_36110.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  30,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "도로명주소_건물"
  },
  {
    "file_name": "Z_KAIS_TL_SPBD_BULD_36110.prj",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "prj",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "도로명주소_건물"
  },
  {
    "file_name": "Z_KAIS_TL_SPBD_BULD_36110.shp",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "shp",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "도로명주소_건물"
  },
  {
    "file_name": "Z_KAIS_TL_SPBD_BULD_36110.shx",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id": "uuid",
    "file_format": "shx",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "도로명주소_건물"
  },
  {
    "file_name": "소상공인시장진흥공단_상가_상권_정보_세종.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  39,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "소상공인시장진흥공단_상가_상권"
  },
  {
    "file_name": "지역별고용조사_A형_시군_중분류.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "지역별고용조사"
  },
  {
    "file_name": "MOCT_LINK.dbf",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "dbf",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_LINK.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_NODE.dbf",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "dbf",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_NODE.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MULTILINK.dbf",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "dbf",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MULTILINK.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "TURNINFO.dbf",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "dbf",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "TURNINFO.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MAPINFO.dbf",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "dbf",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MAPINFO.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "RESTINFO.dbf",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "dbf",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "RESTINFO.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_LINK.prj",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "prj",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_NODE.prj",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "prj",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_LINK.sbn",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "sbn",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_NODE.sbn",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "sbn",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_LINK.sbx",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "sbx",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_NODE.sbx",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "sbx",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_LINK.shp",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "shp",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_NODE.shp",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "shp",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_LINK.shx",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "shx",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "MOCT_NODE.shx",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "shx",
    "encoding": "euc-kr",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "ITS_표준노드링크"
  },
  {
    "file_name": "가구통행실태조사_전국_수단통행기준정렬.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "가구통행실태조사"
  },
  {
    "file_name": "가구통행실태조사_전국_목적통행기준정렬.csv",
    "meta_file_id" : null,
    "staged_file_id": null,
    "dataset_id":  "uuid",
    "file_format": "csv",
    "encoding": "utf-8",
    "number_of_records":  0,
    "number_of_fields":  0,
    "ready_date": null,
    "last_update_time": null,
    "dataset_name": "가구통행실태조사"
  }
]
```

데이터 오브젝트가 않은 이유는 소스로부터 다운로드받은 하나의 데이터 오브젝트로부터 다수의 데이터 오브젝트가 생성되기 때문이다 (예: `NODELINKDATA.zip`)

데이터 소스와 데이터 저장소 또한 각각 데이터 오브젝트 전체 다운로드와 Linux 파일 시스템로 한정하고 있다. 

향후, 이 부분 또한 이전 섹션인 [데이터세트 생성](#create-dataset)와 같이 단순히 카탈로그 테이블에 새로운 행을 삽입하는 것이므로 [SQL의 DML과 같은 명령이나 적어도 API 호출 형식으로 변환](../tech-debts)되어 사용성을 제고하여 일반화하여야 한다.

----
<a id="foot-1" href="#foot-1-ref"><sup>1</sup></a> version 0.1의 경우이며, 향후 더 추가될 수 있음

<a id="foot-2" href="#foot-2-ref"><sup>2</sup></a> 데이터 오브젝트에 대한 구조 정보를 기술하고 있는 메타 데이터인 데이터 오브젝트도 포함.

<a id="foot-3" href="#foot-3-ref"><sup>3</sup></a> version 0.1의 경우 5 데이터세트 모두 다운로드하여 저장하는 데이터 오브젝트만을 지원하고 있으므로 Linux 파일 시스템의 경로로 표시되어 있다. 향후 이는 [확장](../tech-debts)이 필요하다.
