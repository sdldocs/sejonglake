# 프로젝트 개요

본 프로젝트는 세종시 도시행정을 위한 디지털 트윈 구축을 위한 데이터레이크 일부분 프로토타입을 구현하는 것을 목표로 한다.

요구하는 수집 데이터는 5종은 다음과 같다.

- 건물데이터
- 소상공인시장진흥공단_상가(상권) 정보
- MDIS 지역별고용조사 A형 시군 중분류
- ITS 표준노드링크
- 가구통행실태조사

공개된 사이트로 부터 download 방식으로 위의 데이터 세트를 자동과 수동 수집을 병행한다. 자동 수집은 시스템이 주기적으로 사이트를 방문하여 데이터 갱신 여부를 확인하여 갱신되었으면 자동으로 데이터 세트를 다운로드하여 데이터레이크에서 이를 접근할 수 있도록 한다. 수동 수집은 갱신주기가 길거나 다운로드를 위하여 제공 기관으로부터 승인을 얻어야 하는 등의 이유로 운영자가 데이터 세트를 직접 다운로드하여 데이터레이트에 적재하는 것이다.

위의 5 데이터세트중 3종(건물데이터, 소상공인시장진흥공단_상가(상권) 정보, ITS 표준노드링크)은 자동 수집이 이루어지며, 나머지 2종(MDIS 지역별고용조사 A형 시군 중분류, 가구통행실태조사)은 수동으로 수집될 수 있도록 구현되었다.

개발된 데이터레이크 관리 소프트웨어의 핵심 기능은 다음과 같다.

- 데이터 세트 및 데이터에 대한 카탈로그 생성 및 관리
- 데이터 자동 및 수동 수집
- 데이터 변환 및 저장
- 수집된 데이터에 대한 간단한 시각화 기능

